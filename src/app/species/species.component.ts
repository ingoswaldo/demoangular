import {Component, OnInit} from '@angular/core';
import {ConfigService} from "../config/config.service";
import {Specie} from "./specie";
import {NgbPopoverConfig} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-species',
  templateUrl: './species.component.html',
})
export class SpeciesComponent implements OnInit {

    public species: Specie[];
    constructor (private configService: ConfigService, private popover: NgbPopoverConfig) { }

    ngOnInit(): void {
        let service = this.configService;
        this.configService.getSpecies().subscribe(async (data) => {
                const promises = data.map(function (specie) {
                    specie.films.map(function (film, key, films) {
                        service.getFilms(service.getId(film)).subscribe((data) => {
                            films[key] = data
                        });
                    });
                    specie.people.map(function (item, key, people) {
                        service.getPeople(service.getId(item)).subscribe((data) => {
                            people[key] = data
                        });
                    });
                    return specie
                });
                await Promise.all(promises);
                this.species = promises;
            }
        );
        this.popover.placement = "left";
    }
}
