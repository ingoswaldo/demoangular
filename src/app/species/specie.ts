export class Specie {
    constructor(
        public id: string,
        public name: string,
        public classification: string,
        public eye_colors: string,
        public hair_colors: string,
        public people: string[],
        public films: string[]) {}

}
