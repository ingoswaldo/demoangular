import { Component, OnInit } from '@angular/core';
import {ConfigService} from "../config/config.service";
import {Film} from "./film";

@Component({
    selector: 'app-films',
    templateUrl: './films.component.html',
})

export class FilmsComponent implements OnInit{

    public films: Film[];
    constructor (private configService: ConfigService) { }

    ngOnInit(): void {
        let service = this.configService;
        this.configService.getFilms().subscribe(async (data) => {
            const promises = data.map(function (film) {
                film.locations.map(function (location, key, locations) {
                    service.getLocations(service.getId(location)).subscribe((data) => {
                        locations[key] = data;
                    });
                });
                return film;
            });
            await Promise.all(promises);
            this.films = promises;
        });
    }
}