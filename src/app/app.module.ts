import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import { AppComponent } from './app.component';
import { FilmsComponent } from "./films/films.component";
import {ConfigService} from "./config/config.service";
import { LocationsComponent } from './locations/locations.component';
import { PeopleComponent } from './people/people.component';
import { SpeciesComponent } from './species/species.component';
import { VehiclesComponent } from './vehicles/vehicles.component';
import { InicioComponent } from './inicio/inicio.component';

const appRoutes: Routes = [
    { path: '', component: InicioComponent },
    { path: 'films', component: FilmsComponent },
    { path: 'locations', component: LocationsComponent },
    { path: 'people', component: PeopleComponent },
    { path: 'species', component: SpeciesComponent },
    { path: 'vehicles', component: VehiclesComponent },
];

@NgModule({
  declarations: [
    AppComponent, FilmsComponent, LocationsComponent, PeopleComponent, SpeciesComponent, VehiclesComponent, InicioComponent
  ],
  imports: [
      RouterModule.forRoot(
          appRoutes,
          {enableTracing: false} // <-- debugging purposes only
      ),
      BrowserModule, HttpClientModule, RouterModule, NgbModule
  ],
  providers: [
      ConfigService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
