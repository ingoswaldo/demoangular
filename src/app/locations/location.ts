export class Location {
    constructor(
        public id: string,
        public name: string,
        public climate: string,
        public terrain: string,
        public surface_water: string,
        public films: string[]) {}
}
