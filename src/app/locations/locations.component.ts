import { Component, OnInit } from '@angular/core';
import {ConfigService} from "../config/config.service";
import {Location} from "./location";
import {NgbPopoverConfig} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
})
export class LocationsComponent implements OnInit {

    public locations: Location[];
    constructor (private configService: ConfigService, private popover: NgbPopoverConfig) { }

    ngOnInit(): void {
        let service = this.configService;
        this.configService.getLocations().subscribe(async (data) => {
                const promises = data.map(function (location) {
                    location.films.map(function (film, key, films) {
                        service.getFilms(service.getId(film)).subscribe((data) => {
                            films[key] = data
                        });
                    });
                    return location
                });
                await Promise.all(promises);
                this.locations = promises;
            }
        );

        this.popover.placement = "left";
    }
}
