import { Component, OnInit } from '@angular/core';
import {ConfigService} from "../config/config.service";
import {People} from "./people";
import {NgbPopoverConfig} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-peoples',
  templateUrl: './people.component.html'
})
export class PeopleComponent implements OnInit {

    public people: People[];
    constructor (private configService: ConfigService, private popover: NgbPopoverConfig) { }

    ngOnInit(): void {
        let service = this.configService;
        this.configService.getPeople().subscribe(async (data) => {
                const promises = data.map(function (people) {
                    people.films.map(function (film, key, films) {
                        service.getFilms(service.getId(film)).subscribe((data) => {
                            films[key] = data
                        });
                    });
                    return people
                });
                await Promise.all(promises);
                this.people = promises;
            }
        );

        this.popover.placement = "left";
    }

}
