export class People {
    constructor(
        public id: string,
        public name: string,
        public gender: string,
        public age: string,
        public eye_color: string,
        public hair_color: string,
        public films: string[]) { }
}
