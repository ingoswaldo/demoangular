import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class ConfigService {
    private url = 'https://ghibliapi.herokuapp.com/';

    constructor(public http: HttpClient) {}

    public getFilms = (id?: string): any => {
        const tipo = 'films';
        return this.getItems(tipo, id);
    };

    public getId = (string: string): string => {
        return string.slice(string.lastIndexOf('/') + 1);
    };

    public getLocations = (id?: string): any => {
        const tipo = 'locations';
        return this.getItems(tipo, id);
    };

    public getPeople = (id?: string): any => {
        const tipo = 'people';
        return this.getItems(tipo, id);
    };

    public getSpecies = (id?: string): any => {
        const tipo = 'species';
        return this.getItems(tipo, id);
    };
    public getVehicles = (id?: string): any => {
        const tipo = 'vehicles';
        return this.getItems(tipo, id);
    };

    private getItems = (tipo: string, id?: string): any => {
        let url = this.url + tipo;
        url += (id && id !== '') ? `/${id}` : '';
        return this.http.get(url);
    }
}