export class Vehicle {
    constructor(
        public id: string,
        public name: string,
        public description: string,
        public vehicle_class: string,
        public length: string,
        public pilots: string,
        public films: string){}
}
