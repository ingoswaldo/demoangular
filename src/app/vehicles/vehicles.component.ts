import { Component, OnInit } from '@angular/core';
import {People} from "../people/people";
import {ConfigService} from "../config/config.service";
import {Vehicle} from "./vehicle";

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
})
export class VehiclesComponent implements OnInit {

    public vehicles: Vehicle[];
    constructor (private configService: ConfigService) { }

    ngOnInit(): void {
        let service = this.configService;
        this.configService.getVehicles().subscribe(async (data) => {
                const promises = data.map(function (vehicle) {
                    service.getFilms(service.getId(vehicle.films)).subscribe((data) => {
                        vehicle.films = data['title'];
                    });
                    service.getPeople(service.getId(vehicle.pilot)).subscribe((data) => {
                        vehicle.pilots = data['name'];
                    });
                    return vehicle
                });
                await Promise.all(promises);
                this.vehicles = promises;
            }
        );
    }

}
